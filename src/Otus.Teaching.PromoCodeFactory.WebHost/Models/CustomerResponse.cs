﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public required string FirstName { get; set; }
        public required string LastName { get; set; }
        public required string Email { get; set; }
        public required List<PreferenceResponse> Preferences { get; init; }
        //TODO: Добавить список предпочтений
        public required List<PromoCodeShortResponse> PromoCodes { get; set; }
    }
}