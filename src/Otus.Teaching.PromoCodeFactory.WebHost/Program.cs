using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSqlite<DataContext>("Filename=PromoCodes.db", options => options.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.DataAccess"));
builder.Services.AddControllers();
builder.Services.AddScoped<IRepository<Role>, EfRepository<Role>>();
builder.Services.AddScoped<IRepository<Employee>, EfRepository<Employee>>();
builder.Services.AddScoped<IRepository<Preference>, EfRepository<Preference>>();
builder.Services.AddScoped<IRepository<Customer>, EfRepository<Customer>>();
            

builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory API Doc";
    options.Version = "1.0";
});
var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi(settings =>
{
    settings.DocExpansion = "list";

});
            
app.UseHttpsRedirection();

app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MigrateDatabase<DataContext>();
app.Run();
