﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public required string ServiceInfo { get; init; }

        public required string PartnerName { get; init; }

        public required string PromoCode { get; init; }

        public required string Preference { get; init; }
    }
}