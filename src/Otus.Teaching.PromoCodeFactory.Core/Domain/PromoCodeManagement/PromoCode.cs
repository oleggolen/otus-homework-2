﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        public required string Code { get; set; }

        public required string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public required string PartnerName { get; set; }

        public required  Employee PartnerManager { get; init; }

        public required Preference Preference { get; init; }
    }
}