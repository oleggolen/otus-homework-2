﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>(IList<T> data) : IRepository<T>
        where T : BaseEntity
    {
        private IList<T> Data { get; set; } = data;

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public async ValueTask<T?> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        public ValueTask<T> AddAsync(T item)
        {
            Data.Add(item);
            return ValueTask.FromResult(item);
        }

        public Task UpdateAsync(T item)
        {
            return Task.Run(() =>
            {
                var index = Data.IndexOf(item);
                Data[index] = item;
            });
        }

        public Task RemoveAsync(T item)
        {
            return Task.Run(() => { Data.Remove(item); });
        }

    }
}