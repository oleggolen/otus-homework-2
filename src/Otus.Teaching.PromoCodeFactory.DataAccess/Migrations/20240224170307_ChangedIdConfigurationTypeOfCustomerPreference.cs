﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class ChangedIdConfigurationTypeOfCustomerPreference : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("d6211a7f-7cf1-4e45-a99a-d3881287f750"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("dee2b4fd-cc05-4e63-949f-7bd30f5c8535"));

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[,]
                {
                    { new Guid("091e55b8-3779-4040-a82c-f997f80340df"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") },
                    { new Guid("988d40c6-fd5f-45e3-bf55-f008d1bebf35"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("091e55b8-3779-4040-a82c-f997f80340df"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("988d40c6-fd5f-45e3-bf55-f008d1bebf35"));

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[,]
                {
                    { new Guid("d6211a7f-7cf1-4e45-a99a-d3881287f750"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                    { new Guid("dee2b4fd-cc05-4e63-949f-7bd30f5c8535"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") }
                });
        }
    }
}
