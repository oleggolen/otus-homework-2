﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public class CustomerPreference  : BaseEntity
{
    public Guid CustomerId { get; init; }
    public Guid PreferenceId { get; init; }

}