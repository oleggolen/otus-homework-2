﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : BaseEntity
    {
        public required string FirstName { get; set; }
        public required string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public required string Email { get; set; }

        public Role Role { get; init; } = null!;

        public int AppliedPromoCodesCount { get; set; }
        public required Guid RoleId { get; init; }
    }
}