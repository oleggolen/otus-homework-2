﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        ValueTask<T?> GetByIdAsync(Guid id);
        ValueTask<T> AddAsync(T entity);
        Task RemoveAsync(T item);
        Task UpdateAsync(T entity);
    }
}