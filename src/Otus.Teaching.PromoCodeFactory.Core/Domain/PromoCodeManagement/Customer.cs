﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer :BaseEntity
    {
        public required string FirstName { get; set; }
        public required string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public required string Email { get; set; }
        public IEnumerable<Preference> Preferences { get; set; } = null!;
        public IEnumerable<PromoCode>? PromoCodes { get; set; }
        //TODO: Списки Preferences и Promocodes 
    }
}