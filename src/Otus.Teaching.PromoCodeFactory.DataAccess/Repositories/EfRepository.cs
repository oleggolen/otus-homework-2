﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T>(DataContext context) : IRepository<T>
    where T : BaseEntity
{
    public async Task<IEnumerable<T>> GetAllAsync() => await context.Set<T>().ToListAsync();


    public virtual ValueTask<T?> GetByIdAsync(Guid id) => context.FindAsync<T>(id);

    public async ValueTask<T> AddAsync(T entity)
    {
        await context.AddAsync(entity);
        await context.SaveChangesAsync();
        return entity;
    }

    public async Task RemoveAsync(T item)
    {
        context.Remove(item);
        await context.SaveChangesAsync();
    }

    public async Task UpdateAsync(T entity)
    {
        context.Update(entity);
        await context.SaveChangesAsync();
    }
}