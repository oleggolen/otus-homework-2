﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;

public class CustomersConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasKey(c => c.Id);
        builder.HasMany(c => c.Preferences).WithMany().UsingEntity<CustomerPreference>();
        builder.HasMany(customer => customer.PromoCodes).WithOne().OnDelete(DeleteBehavior.Cascade);
        builder.Navigation(c => c.Preferences).AutoInclude();
        builder.Navigation(c => c.PromoCodes).AutoInclude();
        builder.HasData(FakeDataFactory.Customers);
        
    }
}