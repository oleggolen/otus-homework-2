﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController(IRepository<Customer> customersRepository, IRepository<Preference> preferencesRepository) : ControllerBase
    {
        /// <summary>
        /// Получить список всех клиентов из базы данных
        /// </summary>
        /// <returns>Коллекция всех клиентов из базы данных</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            return Ok((await customersRepository.GetAllAsync()).Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));
        }

        /// <summary>
        /// Получить информацию о клиенте со всеми выданными ему промокодами
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns>Возвращает полную информацию о клиенте с выданными ему промокодами</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customersRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            return Ok(new CustomerResponse
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Id = customer.Id,
                Preferences = customer.Preferences.Select(p => new PreferenceResponse()
                {
                    Name = p.Name,
                    Id = p.Id
                }).ToList(),
                PromoCodes = customer.PromoCodes?.Select(p => new PromoCodeShortResponse()
                {
                    Id = p.Id,
                    Code = p.Code,
                    BeginDate = p.BeginDate.ToShortDateString(),
                    EndDate = p.EndDate.ToShortDateString(),
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo,
                }).ToList() ?? []
            });
        }

        /// <summary>
        /// Добавляет клиента в базу данных
        /// </summary>
        /// <param name="request">Информация о клиенте и его предпочтениях</param>
        /// <returns>Результат удаления. StatusCode 404 в случае ошибки, 201 - успешного добавления</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = new List<Preference>();
            foreach (var id in request.PreferenceIds)
            {
                var preference = await preferencesRepository.GetByIdAsync(id);
                if (preference == null)
                    return BadRequest("Incorrect preference Id");
                preferences.Add(preference);
            }

            var customer = new Customer
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Id = Guid.NewGuid(),
                PromoCodes = null,
                Preferences = preferences
            };

            await customersRepository.AddAsync(customer);
            return Created("", customer);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customersRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            var preferences = new List<Preference>();
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await preferencesRepository.GetByIdAsync(preferenceId);
                if (preference == null)
                    return BadRequest("Incorrect preference Id");
                preferences.Add(preference);
            }

            customer.Email = request.Email;
            customer.Preferences = preferences;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            await customersRepository.UpdateAsync(customer);
            return Ok();
        }
        /// <summary>
        /// Удаляет клиента из базы данных вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id">ID клиента для удаления</param>
        /// <returns>Результат удаления. StatusCode 404 в случае ошибки, 200 - успешного удаления</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await customersRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await customersRepository.RemoveAsync(customer);
            return Ok();
        }
    }
}