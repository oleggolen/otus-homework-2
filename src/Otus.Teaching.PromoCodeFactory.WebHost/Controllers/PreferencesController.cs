﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
/// <summary>
/// Предпочтения пользователей
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferencesController(IRepository<Preference> preferencesRepository) : ControllerBase
{
    /// <summary>
    /// Получение спимска всех возможных предпочтений
    /// </summary>
    /// <returns>Список всех возможных предпочтений в системе</returns>
    [HttpGet]
    [ProducesResponseType(200)]
    public async Task<IEnumerable<PreferenceResponse>> GetAllPreferencesAsync() => (await preferencesRepository.GetAllAsync()).Select(p => new PreferenceResponse()
    {
        Name = p.Name,
        Id = p.Id
    });

}