﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeResponse
    {
        public Guid Id { get; init; }
        public required string FullName { get; init; }

        public required string Email { get; init; }

        public required RoleItemResponse Role { get; init; }

        public int AppliedPromocodesCount { get; init; }
    }
}