﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class CascadeDelete : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCodes_Customers_CustomerId",
                table: "PromoCodes");

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("3d23786f-6bb4-4a03-8f11-dc074b81c0f8"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("b6f51c55-46a2-4cd2-9002-12f42d230c4c"));

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[,]
                {
                    { new Guid("d6211a7f-7cf1-4e45-a99a-d3881287f750"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                    { new Guid("dee2b4fd-cc05-4e63-949f-7bd30f5c8535"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCodes_Customers_CustomerId",
                table: "PromoCodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCodes_Customers_CustomerId",
                table: "PromoCodes");

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("d6211a7f-7cf1-4e45-a99a-d3881287f750"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("dee2b4fd-cc05-4e63-949f-7bd30f5c8535"));

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[,]
                {
                    { new Guid("3d23786f-6bb4-4a03-8f11-dc074b81c0f8"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                    { new Guid("b6f51c55-46a2-4cd2-9002-12f42d230c4c"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCodes_Customers_CustomerId",
                table: "PromoCodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id");
        }
    }
}
