﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class PreferenceResponse
{
    public Guid Id { get; init; }
    public required string Name { get; init; }
}